package com.epam.comparator;

import com.epam.model.Bank;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {
    public int compare(Bank o1, Bank o2) {
        return Integer.compare(o1.getAmount(), o2.getAmount());
    }
}
