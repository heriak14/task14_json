package com.epam.model;

public class Bank {
    private String name;
    private String country;
    private String type;
    private Depositor depositor;
    private int amount;
    private double profitability;
    private TimeConstraint timeConstraint;

    public Bank() {
    }

    public Bank(String name, String country, String type, Depositor depositor,
                int amount, double profitability, TimeConstraint timeConstraint) {
        this.name = name;
        this.country = country;
        this.type = type;
        this.depositor = depositor;
        this.amount = amount;
        this.profitability = profitability;
        this.timeConstraint = timeConstraint;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getType() {
        return type;
    }

    public Depositor getDepositor() {
        return depositor;
    }

    public int getAmount() {
        return amount;
    }

    public double getProfitability() {
        return profitability;
    }

    public TimeConstraint getTimeConstraint() {
        return timeConstraint;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDepositor(Depositor depositor) {
        this.depositor = depositor;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setProfitability(double profitability) {
        this.profitability = profitability;
    }

    public void setTimeConstraint(TimeConstraint timeConstraint) {
        this.timeConstraint = timeConstraint;
    }

    @Override
    public String toString() {
        return "\n------------------BANK-------------------\n"
                + "Bank: " + name + ", " + country
                + "\n\tType of deposit: " + type
                + "\n\t" + depositor
                + "\n\tAmount: " + amount
                + "\n\tProfitability: " + profitability + "%"
                + "\n\t" + timeConstraint;
    }
}
