package com.epam.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeConstraint {
    private Date startDate;
    private Date endDate;

    public TimeConstraint() {
    }

    public TimeConstraint(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Time Constraint:"
                + "\n\t\tStart of deposit: " + new SimpleDateFormat("YYYY-MM-dd").format(startDate)
                + "\n\t\tEnd of deposit: " + new SimpleDateFormat("YYYY-MM-dd").format(endDate);
    }
}
