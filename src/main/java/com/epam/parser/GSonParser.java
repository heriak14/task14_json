package com.epam.parser;

import com.epam.model.Bank;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GSonParser implements Parser {
    @Override
    public List<Bank> parse(File json, File schema) throws IOException, ProcessingException {
        if (!JSONValidator.validate(json, schema)) {
            return new ArrayList<>();
        }
        try (InputStream stream = new FileInputStream(json)) {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            String jsonContent = new String(buffer);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            return Arrays.asList(gson.fromJson(jsonContent, Bank[].class));
        }
    }
}
