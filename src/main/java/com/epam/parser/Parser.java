package com.epam.parser;

import com.epam.model.Bank;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface Parser {
    List<Bank> parse(File json, File schema) throws IOException, ProcessingException, ParseException;
}
