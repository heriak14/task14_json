package com.epam.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

class JSONValidator {
    static boolean validate(File json, File jsonSchema) throws IOException, ProcessingException {
            JsonNode content = JsonLoader.fromFile(json);
            JsonNode schema = JsonLoader.fromFile(jsonSchema);
            JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator validator = factory.getValidator();
            ProcessingReport report = validator.validate(schema, content);
            return report.isSuccess();
    }
}
