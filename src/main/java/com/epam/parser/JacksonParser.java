package com.epam.parser;

import com.epam.model.Bank;
import com.epam.model.Depositor;
import com.epam.model.TimeConstraint;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JacksonParser implements Parser{

    @Override
    public List<Bank> parse(File json, File schema) throws IOException, ProcessingException, ParseException {
        if (!JSONValidator.validate(json, schema)) {
            return new ArrayList<>();
        }
        List<Bank> banks = new ArrayList<>();
        try (InputStream stream = new FileInputStream(json)) {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            String jsonContent = new String(buffer);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readValue(jsonContent, JsonNode.class);
            Iterator<JsonNode> iterator = root.elements();
            while (iterator.hasNext()) {
                JsonNode node = iterator.next();
                Bank bank = new Bank();
                Depositor depositor = new Depositor();
                TimeConstraint timeConstraint = new TimeConstraint();
                bank.setName(node.get("name").asText());
                bank.setCountry(node.get("country").asText());
                bank.setType(node.get("type").asText());
                depositor.setName(node.get("depositor").get("name").asText());
                depositor.setAccountID(node.get("depositor").get("accountID").asInt());
                bank.setDepositor(depositor);
                bank.setAmount(node.get("amount").asInt());
                bank.setProfitability(node.get("profitability").asDouble());
                timeConstraint.setStartDate(new SimpleDateFormat("YYYY-MM-dd")
                        .parse(node.get("timeConstraint").get("startDate").asText()));
                timeConstraint.setEndDate(new SimpleDateFormat("YYYY-MM-dd")
                        .parse(node.get("timeConstraint").get("endDate").asText()));
                bank.setTimeConstraint(timeConstraint);
                banks.add(bank);
            }
        }
        return banks;
    }
}
