package com.epam.view;

import com.epam.comparator.BankComparator;
import com.epam.parser.GSonParser;
import com.epam.parser.JacksonParser;
import com.epam.parser.Parser;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final File json = new File(Property.PATH.getProperty("json.path"));
    private static final File schema = new File(Property.PATH.getProperty("json.schema.path"));
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Parser parser;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Parse by GSon data binding");
        menu.put("2", "Parse by Jackson tree model");
        menu.put("Q", "Quit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::parseByGSon);
        methodsMenu.put("2", this::parseByJackson);
        methodsMenu.put("q", this::quit);
    }

    private void showMenu(Map<String, String> menu) {
        LOGGER.trace("------------MENU------------\n");
        menu.forEach((k, v) -> LOGGER.info(k + " - " + v + "\n"));
    }

    private void parseByGSon() {
        parser = new GSonParser();
        showParseRes(parser);
    }

    private void parseByJackson() {
        parser = new JacksonParser();
        showParseRes(parser);
    }

    private void quit() {
    }

    private void showParseRes(Parser parser) {
        try {
            parser.parse(json, schema).stream()
                    .sorted(new BankComparator())
                    .forEach(b -> LOGGER.info(b + "\n"));
        } catch (IOException | ProcessingException | ParseException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void show() {
        String key;
        do {
            showMenu(menu);
            LOGGER.trace("Enter your choice: ");
            key = SCANNER.nextLine().toLowerCase();
            if (methodsMenu.containsKey(key)) {
                methodsMenu.get(key).print();
            } else if (!key.equals("q")) {
                LOGGER.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }
}
